$(document).ready(function () {
  $(window).resize(function () {
    var x = window.matchMedia("(max-width: 1500px)");
    if (x && $(".profile_area").css("display") == "block") {
      $(".left_part").css("width", "55%");
    }
    if ($(".profile_area").css("display") == "block") {
      $(".left_part").css("width", "100%");
    }
  });
  // start collapse
  let check = false;
  $(".collapse").click(function () {
    if (check == false) {
      $(".navigation_bar").css("width", "100px");
      $(".navigation_bar span").hide(200);
      check = true;
    } else {
      $(".navigation_bar span").show(200);
      $(".navigation_bar").css("width", "inherit");
      check = false;
    }
  });
  // end collapse
  // navbar btn click
  $(".chat_btn").click(function () {
    $(".contact_layout").css("display", "none");
    $(".users_layout").css("display", "none");
    $(".conversation_chat").css("display", "flex");
    $(".inner_layout").html("Conversations");
  });
  $(".contact_btn").click(function () {
    $(".conversation_chat").css("display", "none");
    $(".users_layout").css("display", "none");
    $(".contact_layout").css("display", "flex");

    $(".inner_layout").html("Contacts");
  });
  $(".user_btn").click(function () {
    $(".conversation_chat").css("display", "none");
    $(".contact_layout").css("display", "none");
    $(".users_layout").css("display", "flex");
    $(".inner_layout").html("Users");
  });
  // start tap
  $("#tabs li a:not(:first)").addClass("inactive");
  $(".container").hide();
  $(".container:first").show();

  $("#tabs li a").click(function () {
    var t = $(this).attr("id");
    if ($(this).hasClass("inactive")) {
      //this is the start of our condition
      $("#tabs li a").addClass("inactive");
      $("#tabs li a").removeClass("active");
      $(this).addClass("active");
      $(".container").hide(100);
      $("#" + t + "C").fadeIn("slow");
    }
  });
  // end tap
  // open menu delete and replay
  let checkmenu = false;
  $(".arrow").click(function () {
    if (checkmenu == false) {
      $(".action_list").css("display", "block");
      checkmenu = true;
    } else {
      $(".action_list").css("display", "none");
      checkmenu = false;
    }
  });
  $(document).click(function (e) {
    if (!$(e.target).is(".arrow")) {
      $(".action_list").css("display", "none");
    }
  });

  // go to up button
  var btntop = $(".go_to_top");
  var btndown = $(".go_to_down");
  $(".message_area").scroll(function () {
    if ($(".message_area").scrollTop() > 800) {
      btntop.css("display", "block");
    } else {
      btntop.css("display", "none");
    }
    if ($(".message_area").scrollTop() < 500) {
      btntop.css("display", "none");
      btndown.css("display", "block");
    } else {
      btndown.css("display", "none");
    }
  });
  // btn go to up
  btntop.on("click", function (e) {
    e.preventDefault();
    $(".message_area").animate({ scrollTop: 0 }, "300");
  });
  // btn go todown
  btndown.on("click", function (e) {
    e.preventDefault();
    $(".message_area").animate(
      { scrollTop: $(".message_area").prop("scrollHeight") },
      1000
    );
  });
  // scroll to down onload
  $(".message_area").animate(
    { scrollTop: $(".message_area").prop("scrollHeight") },
    1000
  );

  // open profile erea
  $(".data_hint").on("click", function () {
    $(".profile_area").show(500);
  });
  // close profile erea
  $(".close_profile_area").on("click", function () {
    $(".profile_area").hide(200);
  });

  // open replay box
  $(".replay").on("click", function () {
    $(".replay_box").css("display", "flex");
  });
  // open replay box
  $(".doupleclick_div").on("dblclick", function () {
    $(".replay_box").css("display", "flex");
  });
  // close replay box
  $(".close_replay_box").on("click", function () {
    // $(".replay_box").css("display", "none");
    $(".replay_box").hide(200);
  });

  // end conversation layout

  // start contact layout
  $(".dropdown_span , .icon").click(function () {
    $(".menuitems").show(150);
  });
  $(document).click(function (e) {
    if (!$(e.target).is(".dropdown_span")) {
      $(".menuitems").css("display", "none");
    }
  });

  // open import dilog
  $(".import").click(function () {
    $(".import_dilog_container").css("display", "block");
  });
  // close iport dilog
  $(".close_import_dilog").click(function () {
    $(".import_dilog_container").css("display", "none");
  });
  // open new contact dilog
  $(".new_btn").click(function () {
    $(".newContact_dilog_container").css("display", "block");
  });
  // close new contact dilog
  $(".close_import_dilog").click(function () {
    $(".newContact_dilog_container").css("display", "none");
  });
  // open new contact dilog
  $(".newuser_btn").click(function () {
    $(".newUser_dilog_container").css("display", "block");
  });
  // close new contact dilog
  $(".close_import_dilog").click(function () {
    $(".newUser_dilog_container").css("display", "none");
  });

  $(".dark_mood").click(function () {
    document.querySelector("link[href='css/style.css']").href = "css/style-dark.css";
   });
  $(".light_mood").click(function () {
    document.querySelector("link[href='css/style-dark.css']").href = "css/style.css";
  });
  
  // $(".dark_mood").click(function () {
  //   $(".navigation_bar").css("background-color", "#1F2940");
  //   $(".navigation_bar").css(
  //     "border-right",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".layouts_container").css("background", "rgb(31 41 64)");
  //   $(".nav_head").css("background-color", "#1F2940");
  //   $(".user_login").css("background-color", "#1F2940");
  //   $(".user_login").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".conversation_head").css("border-bottom", "0");
  //   $(".search_div").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".conversation").css(
  //     "border-right",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $("#tabs").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".nav_head").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".chatList ul li").css(
  //     "border-bottom",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".archiveList ul li").css(
  //     "border-bottom",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".tab2 ul li").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".labels_with_name ul li .just_label").css(
  //     "border-bottom",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".navigation_bar span").css("color", "#fff");
  //   $(".navigation_bar i").css("color", "#fff");
  //   $(".navigation_bar span").css("color", "#fff");
  //   $(".navigation_bar i").css("color", "#fff");
  //   $(".inner_layout").css("color", "#fff");
  //   $(".data_hint_user").css("color", "#fff");
  //   $(".conversation").css("background-color", "#1f2940");
  //   $(".three-dots").append(
  //     "<style>.three-dots:after{color:#fff !important;}</style>"
  //   ); // after
  //   $("#tabs li a").after().css("background-color", "#1f2940");
  //   $(".info .upper").css("color", "#fff");
  //   $(".info h6").css("color", "#ddd");
  //   $(".name").css("color", "#ddd");
  //   $(".label_name").css("color", "#ddd");
  //   $(".search_header").css("background-color", "#1F2940");
  //   $(".search_header").css(
  //     "border-bottom",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".data_hint").css("color", "#fff");
  //   $(".message_area").css("background-color", "#1f2940");
  //   $(".tab2 ul li").append(
  //     "<style>.tab2 ul li:hover{background-color:#455471 !important;}</style>"
  //   ); // hover
  //   $(".tab2 ul li").append(
  //     "<style>.tab2 ul li:focus{background-color:#35425A !important;}</style>"
  //   ); // focus
  //   $(".chatList ul li").append(
  //     "<style>.chatList ul li:hover{background-color:#455471 !important;}</style>"
  //   ); // hover
  //   $(".chatList ul li").append(
  //     "<style>.chatList ul li:focus{background-color:#35425A !important;}</style>"
  //   ); // focus
  //   $(".archiveList ul li").append(
  //     "<style>.archiveList ul li:hover{background-color:#455471 !important;}</style>"
  //   ); // hover
  //   $(".archiveList ul li").append(
  //     "<style>.archiveList ul li:focus{background-color:#35425A !important;}</style>"
  //   ); // focus
  //   $(".labels_with_name ul li .just_label").append(
  //     "<style>.labels_with_name ul li .just_label:hover{background-color:#455471 !important;}</style>"
  //   ); // hover
  //   $(".labels_with_name ul li .just_label").append(
  //     "<style>.labels_with_name ul li .just_label:focus{background-color:#35425A !important;}</style>"
  //   ); // focus
  //   $(".chat_data").css("color", "#fff");
  //   $(".message_detailes p").css("color", "#fff");
  //   $(".my_message .message_inner").css("color", "#fff");
  //   $(".my_message .message_inner").css("background-color", "rgb(41 54 84)");
  //   $(".my_message .message_inner").css("box-shadow", "none");
  //   $(".my_message .message_inner").css("border", "0");
  //   $(".other_message .message_inner").css("border", "0");
  //   $(".my_message .arrow").css("box-shadow", "none");
  //   $(".other_message .message_inner").css("color", "#000");
  //   $(".other_message .message_inner").css("background-color", "#fff");
  //   $(".other_message .message_inner").css("box-shadow", "none");
  //   $(".write_message").css("box-shadow", "none");
  //   $(".replay_box").css("box-shadow", "none");
  //   $(".write_message").css("border", "none");
  //   $(".replay_box").css("border", "none");
  //   $(".write_message").css("background-color", "#293654");
  //   $(".replay_box").css("background-color", "#293654");
  //   $(".message_replay").css("background-color", "#384664");
  //   $(".message_replay p").css("color", "#fff");
  //   $(".write_message i").css("color", "#b1b1b1");
  //   $(".close_replay_box").css("color", "#b1b1b1");
  //   $(".write_message .tel").css("color", "#fff");
  //   $(".write_message input").css("background-color", "#384664");
  //   $(".date_time").append(
  //     "<style>.date_time:after{background-color:rgba(222, 228, 236, 0.1) !important;}</style>"
  //   ); // after
  //   $(".profile_area").css("background-color", "#1F2940");
  //   $(".profile_area").css("border-left", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".first_part").css("background-color", "#1F2940");
  //   $(".first_part p").css("color", "#fff");
  //   $(".first_part p span").css("color", "#fff");
  //   $(".first_part span").css("color", "#fff");
  //   $(".second_part span").css("color", "#fff");
  //   $(".second_part p").css("color", "#cbcbcb");
  //   $(".three_part > span").css("color", "#fff");
  //   $(".four_part span").css("color", "#fff");
  //   $(".five_part span").css("color", "#fff");
  //   $(".first_part").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".second_part").css(
  //     "border-bottom",
  //     "1px solid rgba(222, 228, 236, 0.1)"
  //   );
  //   $(".three_part").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".four_part").css("border-bottom", "1px solid rgba(222, 228, 236, 0.1)");
  //   $(".group_items ul").css("background-color", "rgb(31 41 64)");
  //   // user Page

  //   $(".contact_container").css("background-color", "#1f2940");
  //   $(".upper_btns_div").css("background-color", "#37496C");
  //   $(".upper_btns_div").css("box-shadow", "none");
  //   $(".second_div").css("background-color", "#37496C");
  //   $(".second_div").css("box-shadow", "none");
  //   $(".select_part label").css("color", "#fff");
  //   $(".header_table").css("color", "#fff");
  //   $("tr").css("background-color", "#f2f2f2");
  //   $(".header_table").css("background-color", "#008491");
  //   $(".dilog .second_div").css("background-color", "transparent");
  //   // report page 
  //   $(".reports").css("background-color", "#1f2940");
  //   $(".reports .header").css("background-color", "#37496B");
  //   $(".reports .header").css("border", "0");
  //   $(".reports .header").css("box-shadow", "none");
  //   $(".reports .header .left_part select").css("box-shadow", "none");
  //   $(".reports .big_card").css("box-shadow", "none");
  //   $(".reports .big_card").css("background-color", "#37496c");
  //   $(".reports .country_with_message").css("box-shadow", "none");
  //   $(".reports .country_with_message").css("background-color", "#1f2940");
  //   $(".reports .chart").css("box-shadow", "none");
  //   $(".reports .country_with_message th").css("color", "#fff");
  //   $(".reports .country_with_message td").css("color", "#fff");
  // });


  // $(".light_mood").click(function () {
  //   $(".navigation_bar").css("background-color", "#023c3f");
  //   $(".layouts_container").css(
  //     "background",
  //     "linear-gradient(90deg, #020024 0%, #d8fffe 0%, #dde7e5 100%)"
  //   );
  //   $(".nav_head").css("background-color", "#1c4f54");
  //   $(".user_login").css("background-color", "#fff");
  //   $(".user_login").css("border-bottom", "1px solid #eee");
  //   $(".search_div").css("border-bottom", "1px solid #eee");
  //   $(".conversation").css("border-right", "1px solid #eee");
  //   $(".chatList ul li").css("border-bottom", "1px solid #eee");
  //   $(".archiveList ul li").css("border-bottom", "1px solid #eee");
  //   $(".tab2 ul li").css("border-bottom", "1px solid #eee");
  //   $(".labels_with_name ul li .just_label").css(
  //     "border-bottom",
  //     "1px solid #eee"
  //   );
  //   $("#tabs").css("border-bottom", "1px solid #eee");
  //   $(".navigation_bar i").css("color", "#7597a2");
  //   $(".navigation_bar span").css("color", "#7597a2");
  //   $(".inner_layout").css("color", "#022c2e");
  //   $(".data_hint_user").css("color", "#4e4e4e");
  //   $(".conversation").css("background-color", "#fff");
  //   $(".three-dots").append(
  //     "<style>.three-dots:after{color:#494949 !important;}</style>"
  //   ); // after
  //   $("#tabs li a").after().css("background-color", "#fff");
  //   $(".info .upper").css("color", "#000");
  //   $(".info h6").css("color", "#545353");
  //   $(".name").css("color", "#646464");
  //   $(".data_hint").css("color", "#4e4e4e");
  //   $(".label_name").css("color", "#646464");
  //   $(".search_header").css("background-color", "#eee");
  //   $(".message_area").css("background-color", "#fff");
  //   $(".tab2 ul li").append(
  //     "<style>.tab2 ul li:hover{background-color:#eee !important;}</style>"
  //   ); // hover
  //   $(".tab2 ul li").append(
  //     "<style>.tab2 ul li:focus{background-color:#e0e0e0 !important;}</style>"
  //   ); // focus
  //   $(".chatList ul li").append(
  //     "<style>.chatList ul li:hover{background-color:#eee !important;}</style>"
  //   ); // hover
  //   $(".chatList ul li").append(
  //     "<style>.chatList ul li:focus{background-color:#e0e0e0 !important;}</style>"
  //   ); // focus
  //   $(".archiveList ul li").append(
  //     "<style>.archiveList ul li:hover{background-color:#eee !important;}</style>"
  //   ); // hover
  //   $(".archiveList ul li").append(
  //     "<style>.archiveList ul li:focus{background-color:#e0e0e0 !important;}</style>"
  //   ); // focus
  //   $(".labels_with_name ul li .just_label").append(
  //     "<style>.labels_with_name ul li .just_label:hover{background-color:#eee !important;}</style>"
  //   ); // hover
  //   $(".labels_with_name ul li .just_label").append(
  //     "<style>.labels_with_name ul li .just_label:focus{background-color:#e0e0e0 !important;}</style>"
  //   ); // focus
  //   $(".chat_data").css("color", "#000");
  //   $(".message_detailes p").css("color", "#000");
  //   $(".my_message .message_inner").css("color", "#000");
  //   $(".my_message .message_inner").css("background-color", "#fff");
  //   $(".my_message .message_inner").css("box-shadow", "0px 3px 3px #ddd");
  //   $(".my_message .message_inner").css("border", "1px solid #ddd");
  //   $(".other_message .message_inner").css("border", "1px solid #ddd");
  //   $(".other_message .message_inner").css("color", "#252525");
  //   $(".other_message .message_inner").css("background-color", "#eee");
  //   $(".other_message .message_inner").css("box-shadow", "0px 3px 3px #ddd");
  //   $(".write_message").css("box-shadow", "0px 0px 8px #ddd");
  //   $(".replay_box").css("box-shadow", "0px 0px 8px #ddd");
  //   $(".write_message").css("border", "1px solid #ddd");
  //   $(".replay_box").css("border", "1px solid #ddd");
  //   $(".write_message").css("background-color", "#f5f4f4");
  //   $(".replay_box").css("background-color", "#f5f4f4");
  //   $(".message_replay").css("background-color", "#E4E4E4");
  //   $(".close_replay_box").css("color", "#000");
  //   $(".write_message i").css("color", "#747474");
  //   $(".write_message input").css("background-color", "#fff");
  //   $(".write_message .tel").css("color", "#fff");
  //   $(".profile_area").css("background-color", "#fff");
  //   $(".first_part").css("background-color", "#fff");
  //   $(".first_part").css("border-bottom", "1px solid #ddd");
  //   $(".second_part").css("border-bottom", "1px solid #ddd");
  //   $(".three_part").css("border-bottom", "1px solid #ddd");
  //   $(".four_part").css("border-bottom", "1px solid #ddd");
  //   $(".first_part p").css("color", "#646464");
  //   $(".first_part span").css("color", "#646464");
  //   $(".date_time").append(
  //     "<style>.date_time:after{background-color:#ddd !important;}</style>"
  //   ); // after
  //   $(".first_part p span").css("color", "#000");
  //   $(".second_part span").css("color", "#969494");
  //   $(".second_part p").css("color", "#474747");
  //   $(".three_part > span").css("color", "#3f3e3e");
  //   $(".four_part span").css("color", "#3f3e3e");
  //   $(".five_part span").css("color", "#3f3e3e");
  //   $(".group_items ul").css("background-color", "#eee");
  //   $(".message_replay p").css("color", "#777676");
  //   // user Page

  //   $(".contact_container").css("background-color", "#f7f7f7");
  //   $(".upper_btns_div").css("background-color", "#f7f7f7");
  //   $(".upper_btns_div").css("box-shadow", "0px 0px 10px #e9e7e7");
  //   $(".dilog .second_div").css("background-color", "transparent");
  //   $(".second_div").css("background-color", "#f7f7f7");
  //   $(".second_div").css("box-shadow", "0px 0px 10px #e9e7e7");
  //   $(".select_part label").css("color", "#505050");
  //   $(".header_table").css("color", "#000");

  //   $("tr").css("background-color", "transparent");
  //   $(".header_table").css("background-color", "#008491");

  //   // report page 
  //   $(".reports").css("background-color", "#fff");
  //   $(".reports .header").css("background-color", "#f7f7f7");
  //   $(".reports .header").css("border", "1px solid #dbdbdb");
  //   $(".reports .header").css("box-shadow", "5px 5px 15px #dbdbdb");
  //   $(".reports .header .left_part select").css("box-shadow", "5px 5px 5px #d5d5d5");
  //   $(".reports .big_card").css("box-shadow", "0px 0px 9px #c9c9c9");
  //   $(".reports .country_with_message").css("box-shadow", "0px 0px 15px #dbdbdb");
  //   $(".reports .chart").css("box-shadow", "0px 0px 15px #dbdbdb");
  //   $(".reports .big_card").css("background-color", "#fff");
  //   $(".reports .country_with_message").css("background-color", "#f7f7f7");
  //   $(".reports .country_with_message ").css("background-color", "#f7f7f7");
  //   $(".reports .country_with_message th").css("color", "#565656");
  //   $(".reports .country_with_message td").css("color", "#565656");
  // });

  // 1px solid rgba(222, 228, 236, 0.1);
});
